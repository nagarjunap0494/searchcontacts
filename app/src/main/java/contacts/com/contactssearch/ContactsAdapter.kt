package contacts.com.contactssearch

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_contacts.view.*

class ContactsAdapter(
    private val context: Context,
    private var mMaincontactsList: ArrayList<ContactVO>) :
    RecyclerView.Adapter<ContactsAdapter.ViewHolder>(), Filterable {

    private var contactListFiltered: List<ContactVO>? = mMaincontactsList

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_contacts,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return contactListFiltered!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.contactName?.text = contactListFiltered!![position].contactName
        holder.contactEmail?.text = contactListFiltered!![position].contactNumber
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val contactName = view.name_textView
        val contactEmail = view.email_textView
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    contactListFiltered = mMaincontactsList
                } else {
                    val filteredList = ArrayList<ContactVO>()
                    for (row in mMaincontactsList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.contactName.toLowerCase().contains(charString.toLowerCase())
                            || row.contactEmail.toLowerCase().contains(charString.toLowerCase())
                            || row.contactNumber.contains(charSequence)
                        ) {
                            filteredList.add(row)
                        }
                    }
                    contactListFiltered = filteredList
                }
                val filterResults = FilterResults()
                filterResults.values = contactListFiltered
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                contactListFiltered = filterResults.values as ArrayList<ContactVO>
                notifyDataSetChanged()
            }
        }
    }
}